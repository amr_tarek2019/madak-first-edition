<?php

use Illuminate\Database\Seeder;

class User extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::create([
            'name'=> 'mostafa',
            'email' => 'mostafa@gmail.com',
            'password' => bcrypt('123456'),
            'phone' => '01011110021',
            'lat'=>'31.0123',
            'lng'=>'29.4567',
            'firebase_token'=>'zxcvbnm123',
            'jwt_token'=>'asdfghjkl456',
            'image'=>'default.png',
            'verify_code'=>'null',
            'user_type'=>'user',
            'notification_status'=>'1',
            'user_status'=>'1',
            'status'=>'1'
        ]);
    }
}
