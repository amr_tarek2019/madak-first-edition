<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreparationsOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preparations_order', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('preparation_booking_id')->unsigned();
            $table->bigInteger('services_sub_sub_category_id')->unsigned();
            $table->string('quantity');

            $table->timestamps();
            $table->foreign('preparation_booking_id')
                ->references('id')->on('preparation_booking')
                ->onDelete('cascade');
            $table->foreign('services_sub_sub_category_id')
                ->references('id')->on('services_sub_sub_categories')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('preparations_order');
    }
}
