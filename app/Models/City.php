<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table='cities';
    protected $fillable=['name_en', 'name_ar'];

    public function getCity()
    {
        return static::all();
    }
}
