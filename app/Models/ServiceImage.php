<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceImage extends Model
{
    protected $table='services_images';
    protected $fillable=['service_id', 'image'];

    public function service()
    {
        return $this->belongsTo('App\Service','service_id');
    }
}
