<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $table='services';
    protected $fillable=['provider_id', 'service_category_id', 'service_subcategory_id',
'services_sub_sub_category_id', 'present_type', 'price', 'quantity', 'lat', 'lng',
'address', 'date_from', 'date_to', 'desc_ar', 'desc_en'];

    public function provider()
    {
        return $this->belongsTo('App\Provider','provider_id');
    }

    public function serviceCategory()
    {
        return $this->belongsTo('App\ServiceCategory','service_category_id');
    }

    public function serviceSubCategory()
    {
        return $this->belongsTo('App\ServiceSubCategory','service_subcategory_id');
    }

    public function servicesSubSubCategory()
    {
        return $this->belongsTo('App\ServicesSubSubCategory','services_sub_sub_category_id');
    }




}
