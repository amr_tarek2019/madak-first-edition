<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table='notifications';
    protected $fillable=['user_id', 'icon', 'title_en', 'title_ar', 'text_en', 'text_ar'];

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
}
