<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlaceImage extends Model
{
    protected $table='places_images';
    protected $fillable=['place_id', 'image'];

    public function place()
    {
        return $this->belongsTo('App\Place','place_id');
    }
}
