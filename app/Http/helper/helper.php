<?php

use Illuminate\Support\Facades\Config;

function success()
{
    return 'success';
}
function error()
{
    return 'error';
}
function expired()
{
    return 'expired';
}


function failed()
{
    return 'failed';
}

function res_msg($status,$code,$key,$data=null)
{

    $response['code'] = $code;
    $response['status']=$status;

    $response['msg'] = Config::get('response.'.$key);

    if ($data!=null){
        $response['data'] = $data;

    }

    return $response;
}


function Curl($url,$fields){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8','Authorization: $auth'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, True);

    return  curl_exec($ch);

}
