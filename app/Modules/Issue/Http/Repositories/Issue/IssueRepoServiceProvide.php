<?php
namespace App\Modules\Issue\Http\Repositories\Issue;


use Illuminate\Support\ServiceProvider;


class IssueRepoServiceProvide extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

    }


    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Modules\Issue\Http\Repositories\Issue\IssueInterface',
            'App\Modules\Issue\Http\Repositories\Issue\IssueRepository');
    }
}
