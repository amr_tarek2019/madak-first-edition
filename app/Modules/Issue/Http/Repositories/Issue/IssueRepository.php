<?php
namespace App\Modules\Issue\Http\Repositories\Issue;
use App\Modules\Issue\Http\Repositories\Issue\IssueInterface as IssueInterface;

use App\Models\Issue;

class IssueRepository implements IssueInterface
{
    public $issue;
    function __construct(Issue $issue) {
        $this->issue = $issue;
    }
    public function getAllIssues()
    {
        return $this->issue->getAllIssues();
    }

}
