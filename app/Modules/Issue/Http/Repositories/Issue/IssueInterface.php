<?php
namespace App\Modules\Issue\Http\Repositories\Issue;

interface IssueInterface{

    public function getAllIssues();

}
