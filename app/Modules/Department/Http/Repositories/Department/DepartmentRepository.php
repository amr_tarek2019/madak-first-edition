<?php
namespace App\Modules\Department\Http\Repositories\Department;
use App\Modules\Department\Http\Repositories\Department\DepartmentInterface as DepartmentInterface;

use App\Models\Department;

class DepartmentRepository implements DepartmentInterface
{
    public $department;
    function __construct(Department $department) {
        $this->department = $department;
    }
    public function getDepartment()
    {
        return $this->department->getDepartment();
    }

}

