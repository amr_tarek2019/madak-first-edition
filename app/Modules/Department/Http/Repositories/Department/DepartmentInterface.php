<?php
namespace App\Modules\Department\Http\Repositories\Department;

interface DepartmentInterface{

    public function getDepartment();

}
