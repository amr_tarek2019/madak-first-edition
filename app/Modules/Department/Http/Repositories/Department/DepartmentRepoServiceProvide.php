<?php
namespace App\Modules\Department\Http\Repositories\Department;


use Illuminate\Support\ServiceProvider;


class DepartmentRepoServiceProvide extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

    }


    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Modules\Department\Http\Repositories\Department\DepartmentInterface',
            'App\Modules\Department\Http\Repositories\Department\DepartmentRepository');
    }
}
