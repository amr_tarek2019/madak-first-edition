<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your module. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//
//Route::get('/department', function (Request $request) {
//    // return $request->department();
//})->middleware('auth:api');
//Route::group(['prefix'=>'/department'], function (){
//    Route::group(['prefix'=>'user'],function () {
//        Route::get('Department', 'DepartmentController@index');
//    });
//});

//Route::get('/department', 'DepartmentController@index');

//
//Route::group(['prefix' => 'department'], function () {
//});
//Route::get('/department', 'Api\DepartmentController@index');
Route::group(['prefix' => 'department'], function () {
    Route::get('', 'Api\DepartmentController@index');
});
