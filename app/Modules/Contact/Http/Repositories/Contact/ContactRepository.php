<?php
namespace App\Modules\Contact\Http\Repositories\Contact;
use App\Modules\Contact\Http\Repositories\Contact\ContactInterface as ContactInterface;

use App\Models\Contact;

class ContactRepository implements  ContactInterface
{
    public $contact;
    function __construct(Contact $contact) {
        $this->contact = $contact;
    }
    public function create(array $attributes){
        return $this->contact->create($attributes);
    }

}

