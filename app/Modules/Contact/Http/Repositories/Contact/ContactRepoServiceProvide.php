<?php
namespace App\Modules\Contact\Http\Repositories\Contact;


use Illuminate\Support\ServiceProvider;


class ContactRepoServiceProvide extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

    }


    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Modules\Contact\Http\Repositories\Contact\ContactInterface',
            'App\Modules\Contact\Http\Repositories\Contact\ContactRepository');
    }
}
