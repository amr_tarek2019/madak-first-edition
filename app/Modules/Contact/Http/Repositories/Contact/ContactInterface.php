<?php
namespace App\Modules\Contact\Http\Repositories\Contact;

interface ContactInterface{

    public function create(array $attributes);

}
