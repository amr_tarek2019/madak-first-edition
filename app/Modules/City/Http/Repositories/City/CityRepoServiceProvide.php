<?php
namespace App\Modules\City\Http\Repositories\City;


use Illuminate\Support\ServiceProvider;


class CityRepoServiceProvide extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

    }


    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Modules\City\Http\Repositories\City\CityInterface',
            'App\Modules\City\Http\Repositories\City\CityRepository');
    }
}
