<?php
namespace App\Modules\City\Http\Repositories\City;
use App\Modules\City\Http\Repositories\City\CityInterface as CityInterface;

use App\Models\City;

class CityRepository implements CityInterface
{
    public $city;
    function __construct(City $city) {
        $this->city = $city;
    }
    public function getCity()
    {
        return $this->city->getCity();
    }

}

