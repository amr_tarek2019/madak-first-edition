<?php
namespace App\Modules\City\Http\Repositories\City;

interface CityInterface{

    public function getCity();

}
